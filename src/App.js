import React from "react";
import "./App.css";
import "bulma/css/bulma.css";
import { Title } from "./components/title";
import { SearchForm } from "./components/searchForm";

class App extends React.Component {
  state = {
    pelis_encontradas: [],
  };

  peliculasEncontradas = (pelis_encontradas) => {
    this.setState({
      pelis_encontradas,
    });

    console.log("---" + this.state.pelis_encontradas[0].Title);
  };
  //creamos el metodo para poder HACER UN MAP de las pelis
  mostraPelisEncontradas = () => {
    const { pelis_encontradas } = this.state; //pelis_encontradas=this.state.pelis_encontradas

    return pelis_encontradas.map((peli) => {
      return <p key={peli.imdbID}>{peli.Title}</p>;
    });
    // eslint-disable-next-line no-unreachable
  };

  render() {
    console.log("render App");

    return (
      <div className="App">
        <Title>Search Movies </Title>
        <div className="SearchForm_formu">
          <SearchForm retornapeliculasApadre={this.peliculasEncontradas} />
        </div>
        {this.state.pelis_encontradas.length === 0 ? (
          <p>Introduce la pelicula a buscar</p>
        ) : (
          this.mostraPelisEncontradas()
        )}
      </div>
    );
  }
}

export default App;
