/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
const API_KEY = "4287ad07";

export class SearchForm extends Component {
  state = {
    inputMovie: "",
  };

  teclandoEnInput = (ev) => {
    this.setState({ inputMovie: ev.target.value });
  };

  formSubmit = (e) => {
    e.preventDefault();
    // console.log(this.state.inputMovie); //veriamos en la consola del navegador
    const { inputMovie } = this.state; //constante local en la funcion,no funciona en render

    //BUSCAREMOS UN SERVICIO o API de PELICULAS
    fetch(`http://www.omdbapi.com/?apikey=${API_KEY}&s=${inputMovie}`)
      .then((res) => res.json())
      .then((results) => {
        // console.log(results.Search);
        //EN ESTE COMPONENTE SOLO SE PUEDE HACER LA BUSQUEDA: los resultados iran en otro componente
        // lo pondremos el padre App
        //1)sacaremos  2 datos del results
        const { Search, totalResults } = results; // search=results.search
        //2)lo enviaremos mediante la FUNCION PROPS  AL PADRE

        this.props.retornapeliculasApadre(Search);

        // const { Search = [], totalResults = "0" } = results
        // console.log({ Search, totalResults })
        // this.props.onResults(Search)
      });
  };

  render() {
    console.log("render Form");

    return (
      <form onSubmit={this.formSubmit}>
        <div className="field has-addons">
          <div className="control">
            <input
              className="input"
              type="text"
              onChange={this.teclandoEnInput}
              placeholder="movie...."
            />
          </div>
          <div className="control">
            <button className="button is-info">Search </button>
          </div>
        </div>
      </form>
    );
  }
}
