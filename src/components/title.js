import React from "react";

// COMPONENET FUNCIONAL PURO
export const Title = ({ children }) => <h1 className="title">{children}</h1>;
